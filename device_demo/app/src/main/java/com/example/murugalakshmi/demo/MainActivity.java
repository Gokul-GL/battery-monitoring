package com.example.murugalakshmi.demo;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    //int values;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView;
        textView = findViewById(R.id.text);
        //fetch the device name
        String model = Build.MODEL;
        String reqString =Build.DEVICE;


        textView.setText(reqString);
    }

}
