package com.example.murugalakshmi.demo;

import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.util.EntityUtils;

public class MyService extends Service {
    int values;

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // do your jobs here




        BroadcastReceiver br = new BroadcastReceiver() {
            int scale = -1;
            int level = -1;
            int voltage = -1;
            int temp = -1;

            @Override
            public void onReceive(Context context, Intent intent) {
                level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
                voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
                values = level;
                new MyService.DataAsyncTask().execute();
               // Toast.makeText(MyService.this,"battery"+level+""+voltage,Toast.LENGTH_LONG).show();
                Log.v("hello", String.valueOf(level));
                Log.v("hai","Battery monitoring application");
            }
        };
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(br, filter);


        return super.onStartCommand(intent, flags, startId);
    }


    public class DataAsyncTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override 
        protected String doInBackground(String... params) {
            try {
                String postUrl = "http://battery.sasurieinfo.tech/receiver/receiver.php";
                HttpClient httpClient = new DefaultHttpClient();

// post header
                HttpPost httpPost = new HttpPost(postUrl);




// add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                //nameValuePairs.add(new BasicNameValuePair("action", "lenevo"));
// Build.DEVICE is used to fetch the device name
                nameValuePairs.add(new BasicNameValuePair("action", Build.DEVICE));

                nameValuePairs.add(new BasicNameValuePair("action1", "" + values));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


// execute HTTP post request
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity resEntity = response.getEntity();

                if (resEntity != null) {

                    String responseStr = EntityUtils.toString(resEntity).trim();
                    Log.v("OP", "Response: " + responseStr);

                    // you can add an if statement here and do oth

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}